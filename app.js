const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Az üzeneteket tároló tömb
const messages = [];

// Főoldal
app.get('/', (req, res) => {
  res.send(`
    <h1>Üzenet hozzáadása</h1>
    <form action="/addMessage" method="post">
      <input type="text" name="message" />
      <button type="submit">Hozzáadás</button>
    </form>
    <h1>Üzenetek listája</h1>
    <ul>
      ${messages.map(message => `<li>${message}</li>`).join('')}
    </ul>
  `);
});

// Üzenet hozzáadása
app.post('/addMessage', (req, res) => {
  const message = req.body.message;
  messages.push(message);
  res.redirect('/');
});

// Szerver indítása a 3000-es porton
app.listen(3000, () => {
  console.log('Az alkalmazás fut a http://localhost:3000 címen.');
});
